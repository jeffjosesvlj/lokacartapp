package com.mobile.ict.cart.TourCheck;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mobile.ict.cart.activity.DashboardActivity;
import com.mobile.ict.cart.activity.ProfileActivity;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.fragment.ProcessedOrderFragment;
import com.mobile.ict.cart.fragment.ProfileFragment;
import com.mobile.ict.cart.gcm.MyGcmListenerService;
import com.mobile.ict.cart.gcm.RegistrationIntentService;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.SharedPreferenceConnector;

import java.util.ArrayList;

public class DemoLists extends AppCompatActivity {

    FragmentManager fragmentManager;
    NavigationView navigationView;
    public static Boolean backpress = false;
    RelativeLayout relativeLayout;
    AccountHeaderBuilder accountHeaderBuilder;
    Toolbar toolbar;
    MenuItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);


        backpress = false;
        Master.initialise(getApplicationContext());

        Master.productList = new ArrayList<>();
        Master.getJSON = new GetJSON();

        setContentView(R.layout.activity_demo_dashboard);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setSupportActionBar(toolbar);

        relativeLayout = (RelativeLayout) findViewById(R.id.dashboardRelativeLayout);

        fragmentManager = getSupportFragmentManager();



            fragmentManager.beginTransaction().replace(R.id.content_frame, new DemoProductFragment(), Master.PRODUCT_TAG).commit();


    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        //code for search fucntionality


        getMenuInflater().inflate(R.menu.skip, menu);


        item = menu.findItem(R.id.skip);



        return true;

    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        Log.e("Dash act", "menu click: " + item.toString() + " " + item.getItemId());

        switch (item.getItemId()) {

            case R.id.skip:
                SharedPreferenceConnector.writeBoolean(this, Master.INTRO, false);


                if(getIntent().hasExtra("redirectto")){
                    String redirectto = getIntent().getStringExtra("redirectto");
                    Intent i;
                    switch(redirectto) {

                        case "profile": {
                            //go to profile activity
                            i = new Intent(this, ProfileActivity.class);
                            startActivity(i);
                            finish();
                            break;
                        }
                        case "organisation": {
                            //change organisation is a fragment and can't be loaded here so,
                            //go to dashboard activity and tell it to load change organisation fragment
                            i = new Intent(this, DashboardActivity.class);
                            i.putExtra("redirectto", "organisation");
                            startActivity(i);
                            finish();
                            break;
                        }
                        case "dashboard": {
                            //i = new Intent(SplashScreenActivity.this, DashboardActivity.class);
                            i = new Intent(this, DashboardActivity.class);
                            i.putExtra("redirectto" , redirectto);
                            startActivity(i);
                            finish();
                            break;
                        }
                    }


                }else {
                    startActivity(new Intent(this, DashboardActivity.class));
                    finish();
                }

                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


    @Override
    public void onBackPressed() {

        System.exit(0);
        

    }

}