package com.mobile.ict.cart.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Scroller;
import android.widget.Toast;

import com.mobile.ict.cart.R;
import com.mobile.ict.cart.TourCheck.DemoLists;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.util.Validation;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by siddharthsingh on 14/5/16.
 */
public class ProfileActivity extends Activity implements View.OnClickListener{

    ImageView ivDone;
    String localFName, localLName, localAddress, localPincode, localEmail;
    DBHelper dbHelper;
    TextInputEditText eFName, eLName, eAddress, ePincode , eEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        setTitle(R.string.title_fragment_profile);
        
        dbHelper = new DBHelper(this.getApplicationContext());
        eEmail = (TextInputEditText)findViewById(R.id.eEditEmail);
        eFName = (TextInputEditText) findViewById(R.id.eEditProfileFName);
        eLName = (TextInputEditText) findViewById(R.id.eEditProfileLName);
        ePincode = (TextInputEditText) findViewById(R.id.eEditProfilePincode);

        eAddress = (TextInputEditText) findViewById(R.id.eEditProfileAddress);
        /*eAddress.setScroller(new Scroller(this));
        eAddress.setVerticalScrollBarEnabled(true);*/
        eAddress.setMovementMethod(new ScrollingMovementMethod());

        ivDone = (ImageView) findViewById(R.id.ivDone);
        ivDone.setOnClickListener(this);

        init();
    }



    @Override
    protected void onResume() {
        super.onResume();
        Master.getMemberDetails(ProfileActivity.this);
    }

    @Override
    public void onClick(View v) {
        saveData();
    }

    void init()
    {
        if(MemberDetails.getFname().equals("null"))
            eFName.setText("");
        else
            eFName.setText(MemberDetails.getFname());


        if(MemberDetails.getLname().equals("null"))
            eLName.setText("");
        else
            eLName.setText(MemberDetails.getLname());


        if(MemberDetails.getAddress().equals("null"))
            eAddress.setText("");
        else
            eAddress.setText(MemberDetails.getAddress());


        if(MemberDetails.getPincode().equals("null"))
            ePincode.setText("");
        else
            ePincode.setText(MemberDetails.getPincode());

        if(MemberDetails.getEmail().equals("null"))
            eEmail.setText("");
        else
            eEmail.setText(MemberDetails.getEmail());

    }

    void saveData()
    {
        localFName = eFName.getText().toString().trim();
        localLName = eLName.getText().toString().trim();
        localAddress = eAddress.getText().toString().trim();
        localPincode = ePincode.getText().toString().trim();
        localEmail = eEmail.getText().toString().trim();

        if(localFName.equals("") || localLName.equals("") || localAddress.equals("") || localPincode.equals("")||localEmail.equals(""))
        {
            if(localFName.equals(""))
                eFName.setError(getString(R.string.error_required));
            if(localLName.equals(""))
                eLName.setError(getString(R.string.error_required));
            if(localAddress.equals(""))
                eAddress.setError(getString(R.string.error_required));
            if(localPincode.equals(""))
                ePincode.setError(getString(R.string.error_required));

            if(localEmail.equals(""))
                eEmail.setError(getString(R.string.error_required));


        }
        else if(localPincode.length() != 6)
            Toast.makeText(this, R.string.toast_please_enter_a_valid_six_digit_pincode, Toast.LENGTH_LONG).show();
        else if(!Validation.isValidEmail(localEmail)){
            Toast.makeText(this, "Enter valid email", Toast.LENGTH_LONG).show();
        }
        else 
        {
            JSONObject jsonObject = new JSONObject();
            try
            {
                jsonObject.put(Master.FNAME, eFName.getText().toString().trim());
                jsonObject.put(Master.LNAME, eLName.getText().toString().trim());
                jsonObject.put(Master.ADDRESS, eAddress.getText().toString().trim());
                jsonObject.put(Master.PINCODE, ePincode.getText().toString().trim());
                jsonObject.put(Master.MOBILENUMBER, "91" + MemberDetails.getMobileNumber());
                if(!MemberDetails.getEmail().equals("null")){
                    jsonObject.put("oldemail", MemberDetails.getEmail());
                }else {
                    jsonObject.put("oldemail", "91" + MemberDetails.getMobileNumber() + "@gmail.com");
                }
                jsonObject.put(Master.EMAIL,eEmail.getText().toString().trim());

                new EditDetailsTask().execute(jsonObject);
            }
            catch (JSONException e)
            {
                Toast.makeText(ProfileActivity.this, R.string.alert_something_went_wrong, Toast.LENGTH_LONG).show();
            }
        }
    }

    class EditDetailsTask extends AsyncTask<JSONObject, String, String>
    {
        @Override
        protected void onPreExecute() {
            Material.circularProgressDialog(ProfileActivity.this, getString(R.string.pd_sending_data_to_server), true);
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            Master.getJSON = new GetJSON();
//            System.out.println("---URL: " + Master.getEditProfileURL());
//            System.out.println("---JSON: " + params[0]);
            Master.response = Master.getJSON.getJSONFromUrl(Master.getEditProfileURL(), params[0], "POST", true,
                    "ruralict.iitb@gmail.com", "password");
//            System.out.println(Master.response);
            return Master.response;
        }

        @Override
        protected void onPostExecute(String s) {

            if(s.equals("exception"))
            {
                Material.alertDialog(ProfileActivity.this, ProfileActivity.this.getString(R.string.alert_cannot_connect_to_the_server), "OK");
            }
            else
            {
                try {
                    Master.responseObject = new JSONObject(s);
                }
                catch (Exception e)
                {
//                    Log.e("Prof act", "JSON prob");
                    Toast.makeText(ProfileActivity.this, R.string.alert_something_went_wrong, Toast.LENGTH_LONG).show();
                }
                try {
                    if(Master.responseObject.getString(Master.RESPONSE).equals("Success"))
                    {
                        Toast.makeText(ProfileActivity.this, R.string.toast_profile_details_updated_successfully, Toast.LENGTH_LONG).show();

                        MemberDetails.setFname(eFName.getText().toString().trim());
                        MemberDetails.setLname(eLName.getText().toString().trim());
                        MemberDetails.setAddress(eAddress.getText().toString().trim());
                        MemberDetails.setPincode(ePincode.getText().toString().trim());
                        MemberDetails.setEmail(eEmail.getText().toString().trim());


//                        System.out.println("edit task------------" + MemberDetails.getFname() + "---------" + MemberDetails.getLname() + "---------" + MemberDetails.getAddress() + "---------" + MemberDetails.getPincode());

                        dbHelper.updateProfile(MemberDetails.getMobileNumber());

                       /* Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
                        i.putExtra("redirectto" , "organisation");
                        i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(i);
                        finish();*/

                        Intent i = new Intent(getApplicationContext(), DemoLists.class);
                        i.putExtra("redirectto", "organisation");
                        i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(i);
                        finish();

                    }
                    else
                    {
                        Toast.makeText(ProfileActivity.this, R.string.alert_cannot_connect_to_the_server, Toast.LENGTH_LONG).show();
                    }
                }
                catch (JSONException e)
                {
                    Toast.makeText(ProfileActivity.this, R.string.alert_something_went_wrong, Toast.LENGTH_LONG).show();
                }

            }
            if(Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.exit(0);
    }
}
