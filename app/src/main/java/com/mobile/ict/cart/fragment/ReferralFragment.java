package com.mobile.ict.cart.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mobile.ict.cart.LokacartApplication;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.Organisations;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.JSONDataHelper;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.util.SharedPreferenceConnector;
import com.mobile.ict.cart.util.Validation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Siddharthsingh on 06-05-2016.
 */

public class ReferralFragment extends Fragment {

    DBHelper dbHelper;
    EditText phoneNumberNew;
    //EditText emailNew;
    Spinner spinner;
    Button refer;
    ArrayAdapter<String> adapter;
    List<String> list;
    int position = 0;
    private Tracker mTracker;
    private static final String TAG = "ReferralFragment";
    String name = "Referral";
    ImageView contactPickerIV;
    JSONObject obj;
    String Orgabbr;
    String phoneNumber;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        AnalyticsFragment();

        View view = inflater.inflate(R.layout.fragment_referral, container, false);
        dbHelper = new DBHelper(getActivity());
        getActivity().setTitle(R.string.title_fragment_referral);

        setHasOptionsMenu(true);

        phoneNumberNew = (EditText) view.findViewById(R.id.phonenumber_et);
       // emailNew = (EditText) view.findViewById(R.id.email_et);
        refer = (Button) view.findViewById(R.id.refer_button);
        spinner = (Spinner) view.findViewById(R.id.referra_org_spinner);
        //contactPickerIV = (ImageView)view.findViewById(R.id.contact_picker_iv);

        list = new ArrayList<String>();
        list.add(getActivity().getString(R.string.label_loading_org_list));

       /* contactPickerIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(contactPickerIntent, 1);
            }
        });*/


        adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, list);
        spinner.setAdapter(adapter);


        if (Master.isNetworkAvailable(getActivity())) {

            try {
                JSONObject obj = new JSONObject();
                obj.put(Master.MOBILENUMBER, "91" + MemberDetails.getMobileNumber());
                obj.put(Master.PASSWORD, MemberDetails.getPassword());
                new GetOrganisationsTask().execute(obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {

            Toast.makeText(getActivity(), R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();
            refer.setEnabled(false);

        }


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                position = pos;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //check if the feedbcak edit text has any text
                if (phoneNumberNew.getText().toString().trim().equals("")
//                        || emailNew.getText().toString().trim().equals("")
                        ) {

                    AnalyticsEvent("UtilityError", "ReferralError- phone number not present");
                    Toast.makeText(getActivity(), R.string.toast_please_fill_all_the_details, Toast.LENGTH_LONG).show();
                } else if (phoneNumberNew.getText().toString().trim().length() != 10) {

                    AnalyticsEvent("UtilityError", "ReferralError - phone number not 10 digits");
                    Toast.makeText(getActivity(), R.string.toast_enter_valid_number, Toast.LENGTH_LONG).show();


//                } else if (!Validation.isValidEmail(emailNew.getText().toString().trim())) {
//                    AnalyticsEvent("UtilityError", "ReferralError");
//                    Toast.makeText(getActivity(), R.string.toast_enter_valid_emailid, Toast.LENGTH_LONG).show();
                } else {



                    if(!phoneNumberNew.getText().toString().trim().matches("\\d{10}"))
                    {
                        AnalyticsEvent("UtilityError", "ReferralError - phone number not 10 digits");
                        Toast.makeText(getActivity(), R.string.toast_enter_valid_number, Toast.LENGTH_LONG).show();

                    }

                        else
                    {

                        //chech if select organisation is selected in spinner
                        if (position != 0) {
                            Orgabbr = Organisations.organisationList.get(position - 1).getOrgabbr();
                             phoneNumber = "91" + phoneNumberNew.getText().toString().trim();
                           //  String email = emailNew.getText().toString().trim();

                            obj = new JSONObject();
                            sendReferal();

                           /* try {
                                obj = new JSONObject();
                                sendReferal();

                                //email of the new person
                                if(emailNew.getText().toString().trim().equals(""))
                                {
                                   sendReferal();
                                }
                                else
                                {
                                    if (Validation.isValidEmail(emailNew.getText().toString().trim()))
                                    {
                                        obj.put("email", email);
                                        sendReferal();
                                    }
                                    else
                                    {
                                        Toast.makeText(getActivity(), R.string.toast_enter_valid_emailid, Toast.LENGTH_LONG).show();

                                    }

                                }

                            } catch (JSONException e) {

                                e.printStackTrace();
                            }*/
                        } else {
                            //show message to choose organisation
                            Toast.makeText(getActivity(), R.string.toast_please_select_an_organisation, Toast.LENGTH_LONG).show();
                        }

                    }

                }
            }
        });
        return view;
    }


    public void sendReferal()
    {
        //org abbr of the organisation referring to
        try {
            obj.put(Master.DEFAULT_ORG_ABBR, Orgabbr);
            //phone number of new member
            obj.put("phonenumber", phoneNumber);
            //email of the person who is referring the new member
            obj.put("refemail", MemberDetails.getEmail());

              new SendReferralTask().execute(obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    protected void AnalyticsEvent(String categoryId, String actionId) {
        // Get tracker.
        Tracker t = ((LokacartApplication) getActivity().getApplication()).getTracker(
                LokacartApplication.TrackerName.APP_TRACKER);
        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder()
                .setCategory(categoryId)
                .setAction(actionId)
                //                                .setLabel(getString(labelId))
                .build());
    }




  /*  @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check whether the result is ok
        if (resultCode == getActivity().RESULT_OK) {
            // Check for the request code, we might be usign multiple startActivityForReslut
            switch (requestCode) {
                case 1:
                    contactPicked(data);
                    break;
            }
        } else {
            Log.e("Referral froagment", "Failed to pick contact");
        }
    }*/



    /**
     * Query the Uri and read contact details. Handle the picked contact data.
     * @param data
     */
    private void contactPicked(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNo = null ;
            String name = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int  nameIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            // Set the value to the textviews

            phoneNumberNew.setText(phoneNo.trim().replaceAll("\\s+",""));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }






    protected void AnalyticsFragment() {
        // Obtain the shared Tracker instance.
        LokacartApplication application = (LokacartApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

        Log.i(TAG, "Setting screen name: " + name);
        mTracker.setScreenName("Fragment~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        AnalyticsFragment();
        Master.getMemberDetails(getActivity());
    }

    public class SendReferralTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Material.circularProgressDialog(getActivity(), "Sending Referral", false);
//            System.out.println("pre execute of send referral");
            dbHelper.getProfile();


        }

        @Override
        protected String doInBackground(JSONObject... params) {
            Map<String, String> articleParams = new HashMap<String, String>();

            //param keys and values have to be of String type
            articleParams.put("sendreferraldatasenttoserver", params[0].toString());
            FlurryAgent.logEvent("sendreferraldatasenttoserver",articleParams);
//            System.out.println(params[0]);
            Master.response = Master.getJSON.getJSONFromUrl(Master.getSendReferURL(), params[0], "POST", true,
                    MemberDetails.getEmail(), MemberDetails.getPassword());
           //System.out.println("Referral Response " + Master.response);
//            Log.d("Send referral Response ", Master.response);
            //can't call Toast as it displays on UI from background thread
            // Toast.makeText(getContext(),Master.response,Toast.LENGTH_SHORT).show();
            return Master.response;

        }

        @Override
        protected void onPostExecute(String response) {

            Map<String, String> articleParams = new HashMap<String, String>();

            //param keys and values have to be of String type
            articleParams.put("sendreferralresponse", response);
            FlurryAgent.logEvent("sendreferralresponse",articleParams);
            if (Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();

         //   System.out.println("------------" + response);
            if (response.equals("exception")) {
                Toast.makeText(getActivity(), R.string.toast_error_while_sending_referral, Toast.LENGTH_LONG).show();
            } else {
                try {
                    JSONObject responseJson = new JSONObject(response);
                    if (responseJson.has("response")) {
                        String resp = responseJson.getString("response");
                        switch (resp.toLowerCase()) {
                            case "already a member": {
                                Toast.makeText(getActivity(), R.string.toast_already_a_member, Toast.LENGTH_SHORT).show();
                                break;
                            }
                            case "success": {
                                Toast.makeText(getActivity(), R.string.toast_member_referred_successfully, Toast.LENGTH_SHORT).show();
                                AnalyticsEvent("Membership", "Referralsuccess");
                                break;
                            }
                           //
                            case "failure": {
                                AnalyticsEvent("Membership", "Referralfailure");
                                if (responseJson.has("reason")) {
                                    Toast.makeText(getActivity(), R.string.toast_error_cannot_refer_admin, Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getActivity(), R.string.toast_error_while_sending_referral, Toast.LENGTH_LONG).show();
                                }

                                break;
                            }


                        }

                        SharedPreferenceConnector.writeString(getActivity().getApplicationContext(), Master.LOGIN_JSON, response);
                    }


                } catch (JSONException e) {
                    Toast.makeText(getActivity(), R.string.toast_error_refer, Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }
//            System.out.println("post execute of send referral");
            dbHelper.getProfile();
        }
    }


    public class GetOrganisationsTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Material.circularProgressDialog(getActivity(), getString(R.string.pd_fetching_data_from_server), false);
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            GetJSON getJson = new GetJSON();
//            System.out.println(params[0]);
            Master.response = getJson.getJSONFromUrl(Master.getLoginURL(), params[0], "POST", false, null, null);
            return Master.response;
        }

        @Override
        protected void onPostExecute(String response) {

            if (Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();

//            System.out.println("------------" + response);
            if (response.equals("exception")) {
                //clear all previous values in the list and show the error
                list.clear();
               // list.add(getActivity().getString(R.string.label_error_loading_organisations));
                Toast.makeText(getActivity(), getString(R.string.label_error_loading_organisations),Toast.LENGTH_SHORT).show();
                adapter.notifyDataSetChanged();
            } else {
                SharedPreferenceConnector.writeString(getActivity().getApplicationContext(), Master.LOGIN_JSON, response);
                Organisations.organisationList = new ArrayList<Organisations>();
                Organisations.organisationList = JSONDataHelper.getOrganisationListFromJson(getActivity(), SharedPreferenceConnector
                        .readString(getActivity().getApplicationContext(), Master.LOGIN_JSON, Master.DEFAULT_LOGIN_JSON));
                list.clear();

                if (Organisations.organisationList.size() > 0) {
                    list.add(getActivity().getString(R.string.label_select_organisation));
                    for (int i = 0; i < Organisations.organisationList.size(); ++i) {
                        list.add(Organisations.organisationList.get(i).getName());
                    }
                    adapter.notifyDataSetChanged();
                } else {
                    list.add(getActivity().getString(R.string.label_error_loading_organisations));
                    adapter.notifyDataSetChanged();
                }
            }
        }
    }
}
