package com.mobile.ict.cart.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.android.gms.gcm.GcmListenerService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.activity.DashboardActivity;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.SharedPreferenceConnector;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by vish on 13/5/16.
 */

public class MyGcmListenerService extends GcmListenerService {

    public static String ACTION = "update";
    private static final String TAG = "MyGcmListenerService";
    public static int broadcastId=100;
    /**
     * Called when message is received.
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        List<NotificationBundle> notifsBundle = new ArrayList<NotificationBundle>();
        String message = data.getString(("Message"));
        String title = data.getString(("Title"));
        int id = Integer.parseInt(data.getString("id"));
        Type listOfTestObject = new TypeToken<List<NotificationBundle>>() {
        }.getType();
        Gson gson = new Gson();

     //   System.out.println("-------Title: " + title + ". Message: " + message + ". ID: " + id);
//

/*
        if(id == 8){
            Log.d("gcm email verification" , "received");
            SharedPreferenceConnector.writeString(getApplicationContext(), Master.emailVerified,"1");

        }else if (id == 4) {
            */
/*Log.e("GCM Listener", "title: " + title + ". Message: " + message);
            sendNotification(message, title);*//*

            if (!prefs.contains("orderNotifs")) {
                NotificationBundle bundle = new NotificationBundle(message, title);
                notifsBundle.add(bundle);
                String s = gson.toJson(notifsBundle, listOfTestObject);

                System.out.println("-------id--------"+id+"----------"+s+"-------simple notification-----");
                editor.putString("orderNotifs", s);
                editor.commit();
                sendProcessedOrderNotification(message, title);
            } else {

                String s = prefs.getString("orderNotifs", "");
                notifsBundle = gson.fromJson(s, listOfTestObject);
                NotificationBundle bundle = new NotificationBundle(message, title);
                notifsBundle.add(bundle);
                String fin = gson.toJson(notifsBundle, listOfTestObject);
                System.out.println("-------id--------"+id+"----------"+fin+"-------style notification-----");

                editor.putString("orderNotifs", fin);
                editor.commit();

                sendProcessedOrderNotificationWithStyle(message, title, notifsBundle);
            }
//            Log.e("GCM Listener", "Size: " + notifsBundle.size());
        }

        else if (id == 5) {
            System.out.println("-------id--------"+id+"--------simple notification-----");

            //sendBroadCastNotification(message, title);

            if (!prefs.contains("broadcastNotifs")) {
                NotificationBundle bundle = new NotificationBundle(message, title);
                notifsBundle.add(bundle);
                String s = gson.toJson(notifsBundle, listOfTestObject);

                System.out.println("-------id--------" + id + "----------" + s + "-------simple notification-----");
                editor.putString("broadcastNotifs", s);
                editor.commit();
                sendBroadCastNotification(message, title);
            } else {

                String s = prefs.getString("broadcastNotifs", "");
                notifsBundle = gson.fromJson(s, listOfTestObject);
                NotificationBundle bundle = new NotificationBundle(message, title);
                notifsBundle.add(bundle);
                String fin = gson.toJson(notifsBundle, listOfTestObject);
                System.out.println("-------id--------"+id+"----------"+fin+"-------style notification-----");
                editor.putString("broadcastNotifs", fin);
                editor.commit();
                sendBroadCastNotificationWithStyle(message, title, notifsBundle);
            }
        }
        else if (id == 6) {
            System.out.println("-------id--------"+id+"--------simple notification-----");

           // sendDeliveredNotification(message, title);

            if (!prefs.contains("deliverdOrderNotifs")) {
                NotificationBundle bundle = new NotificationBundle(message, title);
                notifsBundle.add(bundle);
                String s = gson.toJson(notifsBundle, listOfTestObject);

                System.out.println("-------id--------"+id+"----------"+s+"-------simple notification-----");
                editor.putString("deliverdOrderNotifs", s);
                editor.commit();
                sendDeliveredNotification(message, title);
            } else {

                String s = prefs.getString("deliverdOrderNotifs", "");
                notifsBundle = gson.fromJson(s, listOfTestObject);
                NotificationBundle bundle = new NotificationBundle(message, title);
                notifsBundle.add(bundle);
                String fin = gson.toJson(notifsBundle, listOfTestObject);
                System.out.println("-------id--------"+id+"----------"+fin+"-------style notification-----");

                editor.putString("deliverdOrderNotifs", fin);
                editor.commit();

                sendDelivereOrderdNotificationWithStyle(message, title, notifsBundle);
            }
        }
        else if (id == 7) {
            System.out.println("-------id--------"+id+"--------simple notification-----");

           // sendChangedOrderNotification(message, title);

            if (!prefs.contains("changedOrderNotifs")) {
                NotificationBundle bundle = new NotificationBundle(message, title);
                notifsBundle.add(bundle);
                String s = gson.toJson(notifsBundle, listOfTestObject);

                System.out.println("-------id--------"+id+"----------"+s+"-------simple notification-----");
                editor.putString("changedOrderNotifs", s);
                editor.commit();
                sendChangedOrderNotification(message, title);
            } else {

                String s = prefs.getString("changedOrderNotifs", "");
                notifsBundle = gson.fromJson(s, listOfTestObject);
                NotificationBundle bundle = new NotificationBundle(message, title);
                notifsBundle.add(bundle);
                String fin = gson.toJson(notifsBundle, listOfTestObject);
                System.out.println("-------id--------"+id+"----------"+fin+"-------style notification-----");

                editor.putString("changedOrderNotifs", fin);
                editor.commit();

                sendChangedOrderNotificationWithStyle(message, title, notifsBundle);
            }
        }
*/




        if(id == 8){
           // Log.d("gcm email verification" , "received");
            SharedPreferenceConnector.writeString(getApplicationContext(), Master.emailVerified,"1");

        }else if (id == 4) {
          //  System.out.println("-------id--------"+id+"--------simple processed order notification-----");

            sendProcessedOrderNotification(message, title);

        }

        else if (id == 5) {
           // System.out.println("-------id--------"+id+"--------simple broadcast notification-----");

            sendBroadCastNotification(message, title);



        }
        else if (id == 6) {
           // System.out.println("-------id--------"+id+"--------simple delivered order notification-----");

             sendDeliveredNotification(message, title);


        }
        else if (id == 7) {
           // System.out.println("-------id--------"+id+"--------simple modified order notification-----");

             sendChangedOrderNotification(message, title);


        }
        else if (id == 10) {
           // System.out.println("-------id--------"+id+"--------simple delete order notification-----");

            sendDeleteOrderNotification(message, title);


        }




    }

    private void sendProcessedOrderNotification(String message, String title) {
        /*Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("redirectto", "product");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 4 *//* Request code *//*, intent,
                PendingIntent.FLAG_ONE_SHOT);*/

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.cart_white)
                .setContentTitle("Lokacart - " + title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
               // .setContentIntent(pendingIntent)
                .setGroup("processed_orders");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(4 /* ID of notification */, notificationBuilder.build());
    }


    private void sendBroadCastNotification(String message, String title) {
       /* Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("redirectto", "product");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 5 *//* Request code *//*, intent,
                PendingIntent.FLAG_ONE_SHOT);*/
          broadcastId++;

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.cart_white)
                .setContentTitle("Lokacart - " + title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))             //   .setContentIntent(pendingIntent)
                .setGroup("broadcast_messages");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(broadcastId /* ID of notification */, notificationBuilder.build());
    }




    private void sendDeliveredNotification(String message, String title) {
       /* Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("redirectto", "product");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 6 *//* Request code *//*, intent,
                PendingIntent.FLAG_ONE_SHOT);*/

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.cart_white)
                .setContentTitle("Lokacart - " + title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
               // .setContentIntent(pendingIntent)
                .setGroup("delivered_orders");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(6 /* ID of notification */, notificationBuilder.build());
    }

    private void sendChangedOrderNotification(String message, String title) {
       /* Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("redirectto", "product");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 7 *//* Request code *//*, intent,
                PendingIntent.FLAG_ONE_SHOT);
*/
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.cart_white)
                .setContentTitle("Lokacart - " + title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
               // .setContentIntent(pendingIntent)
                .setGroup("changed_orders");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(7 /* ID of notification */, notificationBuilder.build());
    }


    private void sendDeleteOrderNotification(String message, String title) {
       /* Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("redirectto", "product");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 7 *//* Request code *//*, intent,
                PendingIntent.FLAG_ONE_SHOT);
*/
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.cart_white)
                .setContentTitle("Lokacart - " + title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))             //   .setContentIntent(pendingIntent)

                        // .setContentIntent(pendingIntent)
                .setGroup("delete_orders");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(10 /* ID of notification */, notificationBuilder.build());
    }

/*

    private void sendProcessedOrderNotificationWithStyle(String message, String title, List<NotificationBundle> notifsBundle) {
       */
/* Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("redirectto", "processed");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 4 *//*
*/
/* Request code *//*
*/
/*, intent,
                PendingIntent.FLAG_ONE_SHOT);*//*


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Lokacart - " + title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri);
               // .setContentIntent(pendingIntent);

        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(notifsBundle.size()+" orders processed");
        Iterator<NotificationBundle> iter = notifsBundle.iterator();

        while(iter.hasNext()) {
            NotificationBundle notif = iter.next();
            inboxStyle.addLine(notif.getText());
        }

        notificationBuilder.setStyle(inboxStyle);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(4 */
/* ID of notification *//*
, notificationBuilder.build());
    }

    private void sendBroadCastNotificationWithStyle(String message, String title, List<NotificationBundle> notifsBundle) {
       */
/* Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("redirectto", "product");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 5 *//*
*/
/* Request code *//*
*/
/*, intent,
                PendingIntent.FLAG_ONE_SHOT);*//*


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Lokacart - " + title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri);
               // .setContentIntent(pendingIntent);

        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(notifsBundle.size()+" broadcast messages");
        Iterator<NotificationBundle> iter = notifsBundle.iterator();

        while(iter.hasNext()) {
            NotificationBundle notif = iter.next();
            inboxStyle.addLine(notif.getText());
        }

        notificationBuilder.setStyle(inboxStyle);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(5 */
/* ID of notification *//*
, notificationBuilder.build());
    }

    private void sendDelivereOrderdNotificationWithStyle(String message, String title, List<NotificationBundle> notifsBundle) {
       */
/* Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("redirectto", "delivered");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 6 *//*
*/
/* Request code *//*
*/
/*, intent,
                PendingIntent.FLAG_ONE_SHOT);*//*


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Lokacart - " + title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri);
               // .setContentIntent(pendingIntent);

        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(notifsBundle.size()+" orders delivered");
        Iterator<NotificationBundle> iter = notifsBundle.iterator();

        while(iter.hasNext()) {
            NotificationBundle notif = iter.next();
            inboxStyle.addLine(notif.getText());
        }

        notificationBuilder.setStyle(inboxStyle);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(6 */
/* ID of notification *//*
, notificationBuilder.build());
    }

    private void sendChangedOrderNotificationWithStyle(String message, String title, List<NotificationBundle> notifsBundle) {
       */
/* Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("redirectto", "placed");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 7 *//*
*/
/* Request code *//*
*/
/*, intent,
                PendingIntent.FLAG_ONE_SHOT);*//*


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Lokacart - " + title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri);
              //  .setContentIntent(pendingIntent);

        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(notifsBundle.size()+" orders changed");
        Iterator<NotificationBundle> iter = notifsBundle.iterator();

        while(iter.hasNext()) {
            NotificationBundle notif = iter.next();
            inboxStyle.addLine(notif.getText());
        }

        notificationBuilder.setStyle(inboxStyle);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(7 */
/* ID of notification *//*
, notificationBuilder.build());
    }
*/

}